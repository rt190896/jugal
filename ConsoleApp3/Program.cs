﻿using Oop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PaginationPractice
{
    class Program
    {
        static void Main(string[] args)
        {

            BaseCls baseCls = new BaseCls();
            baseCls.SUM();


                List<Record> records = new List<Record>
        {
            new Record { Id = 1, Name = "RonakSir" },
            new Record { Id = 2, Name = "Mik" },
            new Record { Id = 3, Name = "Jit" },
            new Record { Id = 4, Name = "Pink" },
            new Record { Id = 5, Name = "Anil" },
            new Record { Id = 6, Name = "viral" },
            new Record { Id = 7, Name = "Ramesh" },
            new Record { Id = 8, Name = "Suresh" },
            new Record { Id = 9, Name = "Mahesh" },
            new Record { Id = 10, Name = "Pravesh" },
            new Record { Id = 11, Name = "yuv" },

        };


            Console.Write("Enter the number of records per page: ");
            int recordsPerPage = Convert.ToInt32(Console.ReadLine());

            //int totalPages=11/2; 5;
            int totalPages = records.Count / recordsPerPage;
            if (records.Count % recordsPerPage != 0)
                totalPages++;
            

            for (int page = 1; page <= totalPages; page++)
            {
                Console.WriteLine("Page " + page + ":");
                List<Record> pageRecords = records.Skip((page - 1) * recordsPerPage)
                                                  .Take(recordsPerPage)
                                                  .ToList();

                foreach (var record in pageRecords)
                {
                    Console.WriteLine("Id: " + record.Id + ", Name: " + record.Name);
                }

                Console.WriteLine();
                Console.ReadLine();
            }

        }

        class Record
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }


    }
}