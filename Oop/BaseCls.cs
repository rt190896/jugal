﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oop
{
    public class BaseCls
    {
        public string name = "TR";
        internal int SUM()
        {
            return 0;
        }
    }

    public class ChildCls : BaseCls
    {
        public string name1 = "TR";

        public int sum1()
        {
            return SUM();
        }
    }
}

