﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oop
{
    //public abstract class Abstraction_VB
    //{
    //    public abstract void animalSound();
    //    public void sleep()
    //    {
    //        Console.WriteLine("Zzz");
    //    }
    //}

    //public class ClsTask : Abstraction_VB
    //{
    //    public int Name()
    //    {
    //        sleep();
    //        return 0;
    //    }
    //    public override void animalSound()
    //    {

    //    }
    //}

    // Abstract class
    abstract class Animal
    {
        // Abstract method (does not have a body)
        public abstract void animalSound();
        // Regular method
        public void sleep()
        {
            Console.WriteLine("Zzz");
        }
    }

    // Derived class (inherit from Animal)
    class Pig : Animal
    {
        public override void animalSound()
        {
            Console.WriteLine("The pig says: wee wee");
        }
    }

    //class Program
    //{
    //    static void Main(string[] args)
    //    {
    //        Pig myPig = new Pig(); // Create a Pig object
    //        myPig.animalSound();  // Call the abstract method
    //        myPig.sleep();  // Call the regular method
    //    }
}










