﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Program
    {
        static void Main(string[] args)
        {
            List<Student> students = new List<Student>();
            Student student = new Student("ronak", "ATrivedi", true, "Test@123");
            students.Add(student);
            Student student1 = new Student();
            student1.FirstName = "Rahul";
            student1.LastName = "BTEST";
            students.Add(student1);
            Student student2 = new Student();
            student2.FirstName = "Vaishanvi";
            student2.LastName = "CTEST";
            student2.Today = true;
            student2.Password = "Jugal@123";
            students.Add(student2);
            Student student3 = new Student();
            student3.FirstName = "Vaishanvi";
            student3.LastName = "DTEST";
            student3.City = "surat";
            students.Add(student3);
            Student student9 = new Student("ronak", "ATrivedi", true, "Test@123");
            students.Add(student9);
            Student student10 = new Student("ronak", "ATrivedi", true, "Test@123");
            students.Add(student10);
            Student student11 = new Student("ronak", "ATrivedi", true, "Test@123");
            students.Add(student11);
            Student student12 = new Student("ronak", "ATrivedi", true, "Test@123");
            students.Add(student12);
            Student student13 = new Student("ronak", "ATrivedi", true, "Test@123");
            students.Add(student13);
            Student student14 = new Student("ronak", "ATrivedi", true, "Test@123");
            students.Add(student14);

            //var data = students.OrderByDescending(x => x.FirstName).ThenByDescending(x => x.LastName).ToList();
            //foreach (var item in data)
            //{
            //    Console.WriteLine(item.FirstName + " " + item.LastName);
            //}
            Console.WriteLine("Enter page number:");
            var data1 = Convert.ToInt32(Console.ReadLine());

            PAGE(students, data1);
            var data3 = Convert.ToInt32(Console.ReadLine());
            PAGE(students, data3);

            var data2 = Convert.ToInt32(Console.ReadLine());
            PAGE(students, data2);

            #region TESTWEND
            //var data1 = "select * from Students where firstName=='Rahul' AND LastName=='Trivedi'";
            //var data = students.Where(x => x.FirstName == "Rahul" && x.LastName == "Trivedi").ToList();
            //foreach (var item in data)
            //{
            //    Console.WriteLine(item.FirstName + " " + item.LastName);
            //}

            //if (students.Any(x => x.FirstName == "ronak" && x.Password == "Test@123"))
            //{
            //    Console.WriteLine("Login");
            //}
            //else
            //{
            //    Console.WriteLine("Login fail!");
            //}

            //var data1 = "select * from students where City not in ('Baroda','Abad')";


            //string[] cityList = { "Baroda", "Abad" };

            //var data3 = students.Where(x => !cityList.Contains(x.City));
            //foreach (var item in data3)
            //{
            //    Console.WriteLine(item.City + " " + item.FirstName + " " + item.LastName);
            //}
            #endregion



            #region OLdStudent
            //listExamples.Add(new ListExample()
            //{
            //    FirstName = "R",
            //    LastName = "A",
            //    Email = "T2gmaio.com",
            //    Phone = 12121,
            //    Std = 1
            //});
            //listExamples.Add(new ListExample()
            //{
            //    FirstName = "R4",
            //    LastName = "B",
            //    Email = "T2gmaio.com",
            //    Phone = 12121,
            //    Std = 1
            //});
            //listExamples.Add(new ListExample()
            //{
            //    FirstName = "R",
            //    LastName = "C",
            //    Email = "T2gmaio.com",
            //    Phone = 12121,
            //    Std = 2
            //});
            //listExamples.Add(new ListExample()
            //{
            //    FirstName = "R2",
            //    LastName = "F",
            //    Email = "T2gmaio.com",
            //    Phone = 12121,
            //    Std = 3
            //});
            //listExamples.Add(new ListExample()
            //{
            //    FirstName = "R3",
            //    LastName = "232",
            //    Email = "T2gmaio.com",
            //    Phone = 12121,
            //    Std = 4
            //});
            //var data = "Select TOP 1 * from listExamples where Std=2 order by FirstName DESC ";
            //List<ListExample> data1 = listExamples.FirstOrDefault(x => x.Std == 2).OrderByDescending(x => x.FirstName).ToList();
            //foreach (var item in listExamples.OrderByDescending(obj => obj.FirstName).ToList())
            //{
            //    Console.WriteLine(item.FirstName + " " + item.LastName);
            //}

            //foreach (var item in listExamples.OrderBy(obj => obj.FirstName).ThenByDescending(o => o.LastName).ToList())
            //{
            //    Console.WriteLine(item.FirstName + " " + item.LastName);
            //}



            //var data = listExamples.LastOrDefault().FirstName + " " + listExamples.LastOrDefault().LastName;

            //var dataz = listExamples.FirstOrDefault(x => x.FirstName == "R3").FirstName;
            //Console.WriteLine(data);
            //var data1 = listExamples.Where(x => x.LastName == "T").ToList();
            //foreach (var item in data1)
            //{
            //    Console.WriteLine(item.FirstName + " " + item.LastName);
            //}

            #endregion
            //var data = "Select * from student where age >=13  And age<= 20";

            Console.ReadLine();
        }

        private static void PAGE(List<Student> students, int data1)
        {
            var dataCVB = students.Skip(data1 * 2).Take(2);
            foreach (var item in dataCVB)
            {
                Console.WriteLine(item.FirstName + " " + item.LastName);
            }
        }
    }

    public class Student
    {
        public Student()
        {
            City = "Baroda";
        }
        public Student(string fname, string lname, bool today, string pass)
        {
            FirstName = fname;
            LastName = lname;
            Today = today;
            Password = pass;
            City = "Abad";
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool Today { get; set; }

        public string Password { get; set; }
        public string City { get; set; }

        //public string Email { get; set; }
        //public long Phone { get; set; }
        //public int Std { get; set; }
    }
}
