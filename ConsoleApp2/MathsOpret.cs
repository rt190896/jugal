﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public class MathsOpret
    {
        private string name; // field
        public string Name
        {
            get { return name; }   // get method
            set { name = value; }  // set method }
        }

        public string GetFullName()
        {
            return name + " Trivedi";
        }


        #region MyRegion

        public int a1 = 0;
        public int b2 = 0;
        public MathsOpret(int a, int b)
        {
            a1 = a;
            b2 = b;
        }
        public int getSUMVal()
        {
            int SUM = a1 + b2;
            return SUM;
        }

        private int getSUBVal()
        {
            int SUb = a1 - b2;
            return SUb;
        }

        #endregion


    }
}
